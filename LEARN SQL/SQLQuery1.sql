/*use master;
GO
CREATE DATABASE Sales
ON
(NAME = Sales_data,
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\saledata.mdf',
SIZE = 10MB,
MAXSIZE = 50MB,
FILEGROWTH = 5MB)

LOG ON 
(NAME = Sales_log,
FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL16.MSSQLSERVER\MSSQL\DATA\sales.ldf',
SIZE =  10MB,
MAXSIZE = 25MB,
FILEGROWTH = 5MB);
GO
*/


/*
use sales;
GO
create schema Sales_schema;
create schema Production;
*/

use Sales;
GO
create table Sales_schema.customers
(
customer_id int ,
first_name varchar(15) not null,
last_name varchar(15) not null,
email varchar (50) not null ,
phone varchar(15) , 
state varchar (15) not null,
city varchar (15) not null ,
street varchar (30) not null ,
zip_code varchar (5)

/* to set a constraint */ 
constraint customers_pk primary key (customer_id)
);

create table categories
(category_id int primary key,
category_name varchar(20));
